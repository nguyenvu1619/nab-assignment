import { IsNotEmpty, ValidateNested } from 'class-validator'
import { Type } from 'class-transformer';

export class ItemOrder {
  @IsNotEmpty()
  productId: string;
  @IsNotEmpty()
  quanity: number
}

export class OrderCreatedDto {
  @ValidateNested({ each: true })
  @Type(() => ItemOrder)
  products: ItemOrder[]
}

export class OrderConfirmedDto {
  @IsNotEmpty()
  orderId: string
  @IsNotEmpty()
  version: number
}
