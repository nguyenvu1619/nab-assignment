import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import * as clc from 'cli-color';
import { OrderRepository } from '../../repository/order.repository';
import { GetOrderQuery } from '../impl';

@QueryHandler(GetOrderQuery)
export class GetHeroesHandler implements IQueryHandler<GetOrderQuery> {
  constructor(private readonly repository: OrderRepository) {}

  async execute(query: GetOrderQuery) {
    console.log(clc.yellowBright('Async GetHeroesQuery...'));
    return this.repository.findAll();
  }
}
