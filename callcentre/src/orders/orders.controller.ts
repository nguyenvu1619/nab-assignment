import { Body, Controller, Get,  Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { OrderCreatedCommand } from './commands/impl/orderCreated.command';
import { OrderCreatedDto } from './dtos/createOrder-dto.class';
import { Order } from './models/order.model';
import { GetOrderQuery } from './queries/impl';

@Controller('orders')
export class OrderController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}
  
 
  @Post()
  @UsePipes(new ValidationPipe({ transform: true }))
  async createOrder(@Body() dto: OrderCreatedDto) {
    console.log(dto)
    return this.commandBus.execute(new OrderCreatedCommand(dto));
  }

  @Get()
  async findAll(): Promise<Order[]> {
    return this.queryBus.execute(new GetOrderQuery());
  }

}
