import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { ClientsModule, Transport } from '@nestjs/microservices'

import { EventStoreModule } from '../core/eventstore/eventstore.module'
import { DatabaseModule } from '../core/database/database.module'
import { CommandHandlers } from './commands/handlers';
import { EventHandlers } from './events/handlers';
import { OrderController } from './orders.controller';
import { QueryHandlers } from './queries/handlers';
import { OrderRepository } from './repository/order.repository';
import { OrderSagas } from './sagas/orders.sagas';
import { SyncEventStoreDatabase } from './workers'

@Module({
  imports: [
    CqrsModule,
    EventStoreModule.forFeature(),
    DatabaseModule.forFeature(),
    ClientsModule.register([
      {
        name: 'EVENT_SERVICE',
        transport: Transport.KAFKA,
        options: {
          client: {
            ssl: false,
            clientId: process.env.KAFKA_BROKER_ID,
            brokers: [process.env.KAFKA_BROKER],
          },
          consumer: {
            groupId: 'order-consumer'
          }
        }
         
      }
    ]),
  ],
  controllers: [OrderController],
  providers:[
    OrderRepository,
    ...QueryHandlers,
    ...CommandHandlers,
    ...EventHandlers,
    OrderSagas,
    SyncEventStoreDatabase
  ]
})
export class HeroesGameModule {}
