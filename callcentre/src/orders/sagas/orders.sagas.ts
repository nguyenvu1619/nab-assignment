import { Injectable } from '@nestjs/common';
import {  ofType, Saga } from '@nestjs/cqrs';
import * as clc from 'cli-color';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { OrderCreatedEvent } from '../events/impl/orderCreated.event';
@Injectable()
export class OrderSagas {
  @Saga()
  dragonKilled = (events$: Observable<any>): Observable<any> => {
    return events$
      .pipe(
        ofType(OrderCreatedEvent),
        delay(1000),
        map(event => {
          console.log(clc.redBright('Inside [OrderSagas] Saga', event));
        //transaction handler
        }),
      );
  }
}
