
import { Injectable } from '@nestjs/common'
import { EventStore } from '../../core/eventstore/eventstore'
import { OrderRepository } from '../repository/order.repository'
interface EventStoreStream {
    operationType: string
    fullDocument: EventStoreDocument
}

interface EventStoreDocument {
    sourceId: string
    payload: object
}

function mapEventStoreToDatabaseDocument(eventStoreDocument: EventStoreDocument){
    return {
        _id: eventStoreDocument.sourceId,
        ...eventStoreDocument.payload
    }
}

@Injectable()
export class SyncEventStoreDatabase{
  constructor(private readonly eventstore: EventStore, private readonly repository: OrderRepository,){
      this.eventstore = eventstore
      this.eventstore.watch((data: EventStoreStream) => {
          console.log(data)
          if(data.operationType === 'insert'){
              const databaseDocument = mapEventStoreToDatabaseDocument(data.fullDocument)
              this.repository.updateOrderToDatabase(databaseDocument._id, databaseDocument)
          }
      })
    }
}