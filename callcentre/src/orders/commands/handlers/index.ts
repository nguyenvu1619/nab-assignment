import { OrderCreatedCommandHandler } from './orderCreated.handler';
import { OrderConfirmedCommandHandler } from './orderConfirmed.handler';

export const CommandHandlers = [OrderCreatedCommandHandler, OrderConfirmedCommandHandler];
