import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import * as clc from 'cli-color';
import { OrderRepository } from '../../repository/order.repository';
import { OrderConfirmedCommand } from '../impl/orderConfirmed.handler';

@CommandHandler(OrderConfirmedCommand)
export class OrderConfirmedCommandHandler implements ICommandHandler<OrderConfirmedCommand> {
  constructor(
    private readonly repository: OrderRepository,
    private readonly publisher: EventPublisher,
  ) {}
  async execute(command: OrderConfirmedCommand) {
    console.log(clc.greenBright('OrderConfirmedCommand...'));
    // const { productId } = command;
    const order = this.publisher.mergeObjectContext(
      await this.repository.confirmOrder(command.order),
    );
    order.commit()
    return order
  }
}
