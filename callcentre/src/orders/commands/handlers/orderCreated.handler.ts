import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import * as clc from 'cli-color';
import { OrderRepository } from '../../repository/order.repository';
import { OrderCreatedCommand } from '../impl/orderCreated.command';

@CommandHandler(OrderCreatedCommand)
export class OrderCreatedCommandHandler implements ICommandHandler<OrderCreatedCommand> {
  constructor(
    private readonly repository: OrderRepository,
    private readonly publisher: EventPublisher,
  ) {}
  async execute(command: OrderCreatedCommand) {
    console.log(clc.greenBright('OrderCreatedCommand...'));
    // const { productId } = command;
    const order = this.publisher.mergeObjectContext(
      await this.repository.createOrder(command.order),
    );
    order.commit()
    return {
      status: 'success'
    }
  }
}
