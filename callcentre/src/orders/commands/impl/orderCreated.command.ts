import { ICommand } from "@nestjs/cqrs";
import { OrderCreatedDto } from '../../dtos/createOrder-dto.class'
export class OrderCreatedCommand implements ICommand {
  constructor(
    public readonly order: OrderCreatedDto,
  ) {}
}
