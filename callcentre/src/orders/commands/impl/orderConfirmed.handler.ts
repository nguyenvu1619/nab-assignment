import { ICommand } from "@nestjs/cqrs";
import { OrderConfirmedDto } from '../../dtos/createOrder-dto.class'
export class OrderConfirmedCommand implements ICommand {
  constructor(
    public readonly order: OrderConfirmedDto,
  ) {}
}
