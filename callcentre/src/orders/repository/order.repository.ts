import { Injectable } from '@nestjs/common';

import { Order } from '../models/order.model';
import { Database } from '../../core/database/database'
import { OrderConfirmedDto } from '../dtos/createOrder-dto.class'

@Injectable()
export class OrderRepository {
  constructor(private readonly readStoreHandler: Database) {}
  async findAll(): Promise<Order[]> {
    const ordersFromDatabase = await this.readStoreHandler.find()
    console.log(ordersFromDatabase)
    const order = new Order(undefined)
    order.setData(ordersFromDatabase)
    return ordersFromDatabase.map(orderFromDatabase => {
      const order = new Order(undefined)
      order.setData(ordersFromDatabase)
      return order
    })
  }

  async createOrder(orderDto) : Promise<Order>{
    const order = new Order(undefined);
    order.createOrder(orderDto);
    return order;
  }

  async confirmOrder(orderConfirmDto: OrderConfirmedDto) : Promise<Order>{
    const order = new Order(undefined);
    order.orderConfirmed(orderConfirmDto.orderId, orderConfirmDto.version);
    return order;
  }
  
  async updateOrderToDatabase(orderId, payload){
    const orderInDatabase = await this.readStoreHandler.updateById(orderId, payload)
    console.log(orderInDatabase)
    return orderInDatabase
  }
}
