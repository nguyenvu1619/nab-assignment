import { AggregateRoot } from '@nestjs/cqrs';
import { OrderCreatedEvent } from '../events/impl/orderCreated.event';
import { OrderConfirmedEvent } from '../events/impl/orderConfirmed.event';

export class Order extends AggregateRoot {
  [x: string]: any;
  constructor(private readonly id: string | undefined) {
    super();
  }
  setData(data) {
    this.data = data;
  }
  async createOrder(data){
    this.setData(data)
    this.apply(new OrderCreatedEvent(this.data))
  }
  orderConfirmed(orderId: string, version: number){
    console.log(this.id)
    this.apply(new OrderConfirmedEvent(orderId, 'CONFIRMED', version))
  }
}
