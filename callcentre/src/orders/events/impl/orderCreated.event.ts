import { OrderCreatedDto } from '../../dtos/createOrder-dto.class'
export class OrderCreatedEvent {
    constructor(
      public readonly data: OrderCreatedDto,
    ) {
    }
  }
  