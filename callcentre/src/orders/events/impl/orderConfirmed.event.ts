
export class OrderConfirmedEvent {
    constructor(
      public readonly orderId: string,
      public readonly status: string,
      public readonly version: number,
    ) {
    }
  }
  