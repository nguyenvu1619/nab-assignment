import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import * as clc from 'cli-color';

import { OrderConfirmedEvent } from '../impl/orderConfirmed.event';
import { EventStore } from '../../../core/eventstore/eventstore'
import { STATUS_ORDER_CHANGED } from './type.constant'

@EventsHandler(OrderConfirmedEvent)
export class OrderConfirmedEventHandler
  implements IEventHandler<OrderConfirmedEvent> {
  constructor(private readonly eventstore: EventStore){
    console.log(this.eventstore , 2)
  }
  handle(event: OrderConfirmedEvent) {
    this.eventstore.updateEvent(event.orderId, event.version, {
        eventType: STATUS_ORDER_CHANGED,
        payload: {
            status: "CONFIRMED"
        }
    })
    console.log(clc.greenBright('OrderConfirmedEvent...'));
  }
}