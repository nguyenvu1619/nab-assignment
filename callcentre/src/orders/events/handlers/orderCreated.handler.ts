import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import * as clc from 'cli-color';

import { OrderCreatedEvent } from '../impl/orderCreated.event';
import { EventStore } from '../../../core/eventstore/eventstore'
import { ORDER_CREATED } from './type.constant'


@EventsHandler(OrderCreatedEvent)
export class OrderCreatedEventHandler
  implements IEventHandler<OrderCreatedEvent> {
  constructor(private readonly eventstore: EventStore){}
  handle(event: OrderCreatedEvent) {
    this.eventstore.createEvent({
      eventType: ORDER_CREATED,
      payload: {
        ...event.data,
        status: 'CREATED'
      }
    })
    console.log(clc.greenBright('OrderCreatedEvent...'));
    return 1
  }
}