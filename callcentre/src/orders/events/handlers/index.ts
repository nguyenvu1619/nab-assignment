import { OrderCreatedEventHandler } from './orderCreated.handler';
import { OrderConfirmedEventHandler } from './orderConfirmed.handler';

export const EventHandlers = [OrderCreatedEventHandler, OrderConfirmedEventHandler];
