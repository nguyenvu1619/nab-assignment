import { NestFactory } from '@nestjs/core';
import { ApplicationModule } from './app.module';
import { Transport } from '@nestjs/microservices'

async function bootstrap() {
  const app = await NestFactory.create(ApplicationModule);
   app.connectMicroservice({
    transport: Transport.KAFKA,
    options: {
      client: {
        clientId: 'order',
        brokers: [process.env.KAFKA_BROKER],
      },
      consumer: {
        groupId: 'order-consumer'
      }
  }
  });

  await app.startAllMicroservicesAsync();
  app.listen(3000, () => console.log('Application is listening on port 3000.'));
}
bootstrap();
