import { Module } from '@nestjs/common';
import { HeroesGameModule } from './orders/orders.module';
import { EventStoreModule } from './core/eventstore/eventstore.module'
import { DatabaseModule } from './core/database/database.module'
@Module
({
  imports: [
    EventStoreModule.forRoot(process.env.EVENT_STORE_URI),
    DatabaseModule.forRoot(process.env.READ_DATABASE_URI),
    HeroesGameModule
  ],
})
export class ApplicationModule {}
