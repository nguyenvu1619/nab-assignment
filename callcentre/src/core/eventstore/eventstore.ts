import { Injectable, Inject} from '@nestjs/common';
import { EventStoreClass } from '../eventstore/eventstore.class'
@Injectable()
export class EventStore{
    private eventStore: EventStoreClass;
    private watchCallback: Function
    constructor(@Inject('EVENT_STORE_PROVIDER') eventStore: EventStoreClass) {
        this.eventStore = eventStore;
        this.eventStore.connect().then(() => {
            this.eventStore.watch(this.watchCallback)
        });
        this.eventStore.getModel("Event")
      }
    async findEvent(id){
        return this.eventStore.findEvent(id)
    }
    async createEvent(event){
        return this.eventStore.createEvent(event)
    }
    async updateEvent(sourceId, version, payload){
        return this.eventStore.updateEvent(sourceId, version, payload)
    }
    async watch(callback){
        this.watchCallback = callback
    }
}