import * as mongoose from 'mongoose'

const { Schema, Types} = mongoose

 const eventStoreSchema = new Schema({
    eventType: {
        type: String
    },
    payload: Schema.Types.Mixed,
    sourceId: {
        type: Schema.Types.ObjectId,
        default: Types.ObjectId
    },
    version: {
        type: Number,
        default: 1
    }
});

eventStoreSchema.index({
    sourceId: 1,
    version: 1
},{ unique: true })

export {
    eventStoreSchema
}