import * as mongoose from 'mongoose'
import { eventStoreSchema } from './eventstore.schema'

export class EventStoreClass{
    private client:mongoose.Model<mongoose.Document>
    private url
    constructor(url:string){
      this.url = url
    }
    async connect(){
        return mongoose.connect(this.url).then(() => {
            console.log("connected eventstore...")
        })
    }
    getModel(model){
        this.client = mongoose.model(model, eventStoreSchema)
    }

    async findEvent(id){
        return this.client.findById(id)
    }

    async createEvent(event){
        return this.client.create(event)
    }

    async updateEvent(sourceId, version, event){
        const latestEvent: any = await this.client.findOne({
            sourceId
        }).sort({ version: -1 })
        if(version !== latestEvent.version){
            throw new Error('Update wrong version event')
        }
        
        if(event.payload === latestEvent.payload){
            throw new Error('Duplicate payload')
        }

        return this.client.create({
            ...event,
            version: version + 1,
            sourceId: sourceId
        })
    }
    watch(callback){
        this.client.watch().on('change', data => {
            callback(data)
        })
    }
}