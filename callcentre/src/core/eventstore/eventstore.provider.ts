import { EventStoreClass } from './eventstore.class';

export const eventStoreProviders = [
  {
    provide: 'EVENT_STORE_PROVIDER',
    useFactory: (
        uri?: string,
    ): any => {
          return new EventStoreClass(uri);
    },
    inject: ['EVENT_STORE_URI'],
  },
];
