import { Global, DynamicModule } from '@nestjs/common';
import { EventStore } from './eventstore'
import { eventStoreProviders } from './eventstore.provider';
@Global()
export class EventStoreModule {
  static forRoot(uri: string): DynamicModule {
    return {
      module: EventStoreModule,
      providers: [
        {
          provide: 'EVENT_STORE_URI',
          useValue: uri,
        },
        ...eventStoreProviders,
      ],
      exports: [...eventStoreProviders]
    };
  }

  static forFeature(): DynamicModule {
    return {
      module: EventStoreModule,
      providers: [
        EventStore,
      ],
      exports: [
        EventStore,
      ],
    };
  }
}
