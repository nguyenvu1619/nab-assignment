import { DatabaseClass } from './database.class';

export const databaseProviders = [
  {
    provide: 'DATABASE_PROVIDER',
    useFactory: (
        uri?: string,
    ): any => {
          return new DatabaseClass(uri);
    },
    inject: ['DATABASE_URI'],
  },
];

