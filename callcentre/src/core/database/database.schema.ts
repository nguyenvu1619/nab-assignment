import { Schema} from 'mongoose'

export const databaseSchema = new Schema({
    products: [{
            productId: String,
            quanity: Number
    }],
    status: {
        type: String,
    },
    version: {
        type: Number
    }
});