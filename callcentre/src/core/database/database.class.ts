import * as mongoose from 'mongoose'
import { databaseSchema } from './database.schema'
export class DatabaseClass{
    private client:mongoose.Model<mongoose.Document>
    private url
    constructor(url:string){
      this.url = url
    }
    async connect(){
        return mongoose.connect(this.url).then(() => {
            console.log("connected eventstore...")
        })
    }
    getModel(model){
        this.client = mongoose.model(model, databaseSchema)
    }

    async findById(id){
        return this.client.findById(id)
    }

    async find(){
        return this.client.find()
    }

    async create(data){
        return this.client.create(data)
    }

    async updateById(id, data){
        return this.client.findByIdAndUpdate(id, 
            {
            ...data,
            $inc: {
                version: 1
              }
            }, 
            {
							new: true,
							upsert: true
            }
        )
    }
}