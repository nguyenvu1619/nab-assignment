import { Injectable, Inject} from '@nestjs/common';
import { DatabaseClass } from './database.class'
@Injectable()
export class Database{
    private database: DatabaseClass;
    constructor(@Inject('DATABASE_PROVIDER') database: DatabaseClass) {
        this.database = database;
        this.database.connect();
        this.database.getModel("Order")
      }
    async findById(id){
        return this.database.findById(id)
    }
    
    async find(){
        return this.database.find()
    }

    async create(data){
        console.log(this.database)
        return this.database.create(data)
    }

    async updateById(id, data){
        return this.database.updateById(id, data)
    }
}