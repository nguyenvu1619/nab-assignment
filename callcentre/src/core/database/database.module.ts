import { Global, DynamicModule } from '@nestjs/common';
import { Database } from './database'
import { databaseProviders } from './database.provider';

@Global()
export class DatabaseModule {
  static forRoot(uri: string): DynamicModule {
    return {
      module: DatabaseModule,
      providers: [
        {
          provide: 'DATABASE_URI',
          useValue: uri,
        },
        ...databaseProviders,
      ],
      exports: [...databaseProviders]
    };
  }

  static forFeature(): DynamicModule {
    return {
      module: DatabaseModule,
      providers: [
        Database,
      ],
      exports: [
        Database,
      ],
    };
  }
}
