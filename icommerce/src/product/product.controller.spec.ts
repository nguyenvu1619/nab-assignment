import { Test, TestingModule } from '@nestjs/testing';
import { ClientsModule, Transport } from '@nestjs/microservices'
import { MongooseModule } from '@nestjs/mongoose';
import { ProductsController } from './product.controller';
import { ProductsService } from './product.service';
import { Product, ProductSchema } from './schemas/product.shema';
import { ProductsEventHandler } from '../eventHandler'

import {  FilterProductMock } from '../../test/mock'

describe('ProductsController', () => {
  let productsController: ProductsController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
        imports: [
            MongooseModule.forRoot(process.env.mongoURI),
            MongooseModule.forFeature([{ name: Product.name, schema: ProductSchema }]),
            ClientsModule.register([
              {
                name: 'EVENT_SERVICE',
                transport: Transport.KAFKA,
                options: {
                  client: {
                    ssl: false,
                    clientId: process.env.KAFKA_BROKER_ID,
                    brokers: [process.env.KAFKA_BROKER],
                  },
                  consumer: {
                    groupId: 'order-consumer'
                  }
                }
              }
            ]),
        ],
          controllers: [ProductsController],
          providers: [ProductsEventHandler, ProductsService],
    }).compile();
    productsController = app.get<ProductsController>(ProductsController);
  });

  describe('root', () => {
    it('should return true', async () => {
      expect(await productsController.filter(FilterProductMock)).toHaveProperty('products',        
        expect.arrayContaining([
          expect.objectContaining({  
            name: 'shirt',
            color: 'red',
            price: 12           
          })
        ])
      );
    });
  });
});
