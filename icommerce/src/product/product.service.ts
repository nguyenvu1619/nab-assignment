import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateProductDto } from './dto/create-product.dto';
import { ProductFilterDto, ProductSortDto, ResponseProductDto } from './dto/query-product.dto';
import { Product, ProductDocument } from './schemas/product.shema';
import { ProductsEventHandler } from '../eventHandler'

const  LIMIT_PER_PAGE = 1;
const DUMP_USER_ID = '1'
const toCursorHash = string => Buffer.from(string).toString('base64');

const fromCursorHash = string =>
  Buffer.from(string, 'base64').toString('ascii');

@Injectable()
export class ProductsService {
  constructor(
    @InjectModel(Product.name) private readonly productModel: Model<ProductDocument>,
    private readonly eventHandler: ProductsEventHandler
  ) {
  }
  private cursorQuery(cursor) {
    return cursor
    ? {
      _id: {
        $gt: fromCursorHash(cursor),
      },
    }
    : {};
  }
  private generateReponseFromProducts = (products: ProductDocument[]) =>{
    const hasNextPage = products.length > LIMIT_PER_PAGE;
    const edges = hasNextPage ? products.slice(0, -1) : products;
    return {
          products: edges,
          cursor: hasNextPage ? toCursorHash(
            edges[edges.length - 1]._id.toString(),
          ): null,
          hasNext: hasNextPage
    }
  }

  private generateColorQuery = colors => {
    return {
      color: {
        $elemMatch: { $in: colors }
      }
    }
  }

  private generateFilterQuery =(filter: ProductFilterDto) => {
    const filterQuery: any = { ...filter }
    if(filter?.colors){
      filterQuery.colors = this.generateColorQuery(filter.colors)
    }
    return filterQuery
  }
  async create(createProductDto: CreateProductDto): Promise<Product> {
    const createdCat = new this.productModel(createProductDto);
    return createdCat.save();
  }

  async findAll(): Promise<Product[]> {
    return this.productModel.find().exec();
  }
  async findById(id:string): Promise<Product> {
    return this.productModel.findById(id).exec();
  }
  
  async filter(filter: ProductFilterDto, sort: ProductSortDto, cursor: string): Promise<ResponseProductDto>{
    this.eventHandler.productSearchEvent(null, {
      userId: DUMP_USER_ID,
      ...filter
    })
    const filterQuery = this.generateFilterQuery(filter)
    const products = await this.productModel.find({
      ...this.cursorQuery(cursor),
      ...filterQuery
    })
    .sort({
      ...sort,
      _id: 1
    })
    .limit(LIMIT_PER_PAGE + 1)
    .exec();
    return this.generateReponseFromProducts(products)
  }
  async searchByName(name: string, filter: ProductFilterDto, sort: ProductSortDto, cursor: string): Promise<ResponseProductDto>{
    this.eventHandler.productSearchEvent(null, {
      userId: DUMP_USER_ID,
      ...filter,
      name
    })
    const searchNameArg = {
        $text: { $search: name } 
    }
    const filterQuery = this.generateFilterQuery(filter)
    const products = await this.productModel.find({
      ...this.cursorQuery(cursor),
      ...filterQuery,
      ...searchNameArg
    })
    .sort({
      ...sort,
      _id: 1
    })
    .limit(LIMIT_PER_PAGE + 1)
    .exec();
    return this.generateReponseFromProducts(products)
  }
}