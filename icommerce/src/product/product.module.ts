import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductsController } from './product.controller';
import { ProductsService } from './product.service';
import { Product, ProductSchema } from './schemas/product.shema';
import { ProductsEventHandler } from '../eventHandler'
import { ClientsModule, Transport } from '@nestjs/microservices'

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Product.name, schema: ProductSchema }]),
    ClientsModule.register([
      {
        name: 'EVENT_SERVICE',
        transport: Transport.KAFKA,
        options: {
          client: {
            ssl: false,
            clientId: process.env.KAFKA_BROKER_ID,
            brokers: [process.env.KAFKA_BROKER],
          },
          consumer: {
            groupId: 'order-consumer',
            retry: { retries: 50 }
          }
        }
         
      }
    ]),
],
  controllers: [ProductsController],
  providers: [ProductsEventHandler, ProductsService],
})
export class ProductsModule {}