import { Transform, Type } from 'class-transformer'
import { ProductDocument } from '../schemas/product.shema'

export class ProductFilterDto {
    name?: string;
    price?: number;
    colors?: [string]
}

export class ProductSortDto  {
    createAt?: 1 | -1;
    price?: 1 | -1
}

export class QueryDto {
    @Transform(({ value }) => {
        console.log(value, "value")
        if(typeof value === "string")
            return JSON.parse(value)
        return value
    })
    @Type(() => ProductFilterDto)
    filter: ProductFilterDto;
    @Transform(({ value }) => {
        if(typeof value === "string")
            return JSON.parse(value)
        return value
    })
    @Type(() => ProductSortDto)
    sort?: ProductSortDto;
    cursor?: string
}


export class ResponseProductDto {
    products: ProductDocument[];
    cursor: string;
    hasNext: boolean;
}