export class CreateProductDto {
    readonly name: string;
    readonly price: number;
    readonly colors: [string];
}