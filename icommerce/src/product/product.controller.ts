import { Body, Controller, Get, Post, Query, UsePipes, ValidationPipe, Param } from '@nestjs/common';
import { ProductsService } from './product.service';
import { CreateProductDto } from './dto/create-product.dto';
import { ResponseProductDto, QueryDto } from './dto/query-product.dto';
import {  Product } from './schemas/product.shema';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}
  @Post()
  async create(@Body() createCatDto: CreateProductDto): Promise<Product> {
    const newProduct = await this.productsService.create(createCatDto);
    return newProduct
  }


  @Get()
  async findAll(): Promise<Product[]> {
    return this.productsService.findAll();
  }

  @Get('filter')
  async filter(
    @Query() queryDto: QueryDto, 
    ): Promise<ResponseProductDto> {
      const { filter, sort, cursor} = queryDto
      console.log(queryDto, typeof queryDto)
    return await this.productsService.filter(filter, sort, cursor);
  }

  @Get('searchByName')
  @UsePipes(new ValidationPipe({ transform: true }))
  async searchByName(
    @Query() queryDto: QueryDto,
    @Query("name") name: string
    ): Promise<ResponseProductDto> {
      const { filter, sort, cursor} = queryDto
    return this.productsService.searchByName(name, filter, sort, cursor);
  }

  @Get(":id")
  async findById(@Param('id') id: string ): Promise<Product> {
    return this.productsService.findById(id);
  }
 
}
