import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ProductDocument = Product & Document;

@Schema({ timestamps: true})
export class Product {
  @Prop({ unique: true, required: true })
  name: string;

  @Prop()
  price: number;

  @Prop()
  colors: [string];
  @Prop()
  version: number;
}



export const ProductSchema = SchemaFactory.createForClass(Product).index({ name: 'text'})
