import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductsModule } from './product/product.module';
@Module({
  imports: [
    MongooseModule.forRoot(process.env.mongoURI),
    ProductsModule,
  ],
})

export class AppModule {}