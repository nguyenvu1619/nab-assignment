export class ProductSearchedMessage {
    userId: string;
    productId?: string;
    color?: string;
    price?: number;
    name?: string
}