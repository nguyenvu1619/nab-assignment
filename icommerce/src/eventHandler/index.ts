import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices'
import * as clc from 'cli-color';

import { ProductSearchedMessage } from './message.class'

const DUMP_USER_ID = '1'
@Injectable()
export class ProductsEventHandler {
    constructor(
        @Inject('EVENT_SERVICE') private client: ClientProxy,
      ) {
          
         this.client.connect().then(() => {
            console.log(clc.greenBright('Connected to kafka...'));
         })
        }

        async productSearchEvent(name, filter: ProductSearchedMessage){
        const pattern = "PRODUCT_SEARCHED";
        const payload: ProductSearchedMessage = {
            userId: DUMP_USER_ID,
            name,
            ...filter
        };
        return this.client.emit(pattern, payload)
    }
    productDetailViewed(id){
        const pattern = 'PRODUCT_SEARCHED';
        const payload: ProductSearchedMessage = {
            userId: DUMP_USER_ID,
            productId: id
        };
        return this.client.emit(pattern, payload)
    }
 
}