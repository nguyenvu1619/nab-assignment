import { Test, TestingModule } from '@nestjs/testing';
import { ClientsModule, Transport } from '@nestjs/microservices'
import { ElasticsearchModule } from '@nestjs/elasticsearch'

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SearchService } from './searchService'

import { MockProductSearchMessage } from '../test/mock'

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        ElasticsearchModule.register({
        node: process.env.ELASTICSEARCH_URI 
        }),
        ClientsModule.register([
          {
            name: 'EVENT_SERVICE',
            transport: Transport.KAFKA,
            options: {
              client: {
                ssl: false,
                clientId: process.env.KAFKA_BROKER_ID,
                brokers: [process.env.KAFKA_BROKER],
              },
              consumer: {
                groupId: 'logging-consumer'
              }
            }
             
          }
        ]),
      ],
      controllers: [AppController],
      providers: [SearchService, AppService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return true', async () => {
      expect(await appController.productSearched(MockProductSearchMessage)).toBe(true);
    });
  });
});
