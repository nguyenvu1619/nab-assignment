import { Module } from '@nestjs/common';
import { ElasticsearchModule } from '@nestjs/elasticsearch'
import { ClientsModule, Transport } from '@nestjs/microservices'

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SearchService } from './searchService'
@Module({
  imports: [
    ElasticsearchModule.register({
    node: process.env.ELASTICSEARCH_URI 
    }),
    ClientsModule.register([
      {
        name: 'EVENT_SERVICE',
        transport: Transport.KAFKA,
        options: {
          client: {
            ssl: false,
            clientId: process.env.KAFKA_BROKER_ID,
            brokers: [process.env.KAFKA_BROKER],
          },
          consumer: {
            groupId: 'logging-consumer',
            retry: {
               retries: 50
            }
          }
        }
         
      }
    ]),
  ],
  controllers: [AppController],
  providers: [SearchService, AppService],
})
export class AppModule {}
