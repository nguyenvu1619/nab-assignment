export const INDEX_NAME = 'product-searched'

export const MAPPING = {
    "properties": {
        "productId": { "type": "keyword" },
        "color": { "type": "keyword" },
        "price": { "type": "half_float" },
        "name": { "type": "text"}
      }
}