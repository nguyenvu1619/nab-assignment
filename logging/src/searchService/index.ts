import { Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch'

@Injectable()
export class SearchService {
  constructor(private readonly elasticsearchService: ElasticsearchService) {}

  async createIndex(name, mappings){
    try{
        return await this.elasticsearchService.indices.create({
            index: name,
            body: {
                mappings
            }
        })
    } catch(e){
        console.log('createIndex error ->', e)
    }
  }

  async indexData(indexName, body){
    try{
        return  await this.elasticsearchService.index({
            index: indexName,
                body
            })
    } catch(e){
        console.log('indexData error ->', e)
    }
  }

  async checkIndexExisted(indexName){
    const result = await this.elasticsearchService.indices.exists({
      index: indexName
    })
    return result.body
  }
}