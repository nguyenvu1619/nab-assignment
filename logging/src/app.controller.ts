import { Controller, Get } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';

import { AppService } from './app.service';
import { KafkaMessagePayload, ProductSearchedMessage } from './dto/message.dto'

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @MessagePattern('PRODUCT_SEARCHED')
  async productSearched(@Payload() message: KafkaMessagePayload): Promise<Boolean> {
    const payload: ProductSearchedMessage = message.value
    return this.appService.indexProductSearchedData(payload)
  }
}

