export class ProductSearchedMessage {
    userId: string;
    color?: string;
    price?: number;
    productId?: string;
}

export class KafkaMessagePayload {
    value: ProductSearchedMessage
}