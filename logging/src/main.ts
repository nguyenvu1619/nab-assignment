import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common'
import { Transport } from '@nestjs/microservices'

import { AppModule } from './app.module';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe({
    transform: true,
    transformOptions: {
      enableImplicitConversion: true
    },
}));
app.connectMicroservice({
  transport: Transport.KAFKA,
  options: {
    client: {
      clientId: 'logging',
      brokers: [process.env.KAFKA_BROKER],
    },
    consumer: {
      groupId: 'logging-consumer'
    }
}
});
  await app.startAllMicroservicesAsync();
  app.listen(3000);
  console.log(`Application is running on port 3000`);
}
bootstrap();
