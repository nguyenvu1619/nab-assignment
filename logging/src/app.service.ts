import { Injectable } from '@nestjs/common';
import { SearchService  } from './searchService'
import { INDEX_NAME, MAPPING} from './constant/search.constant'
import {  ProductSearchedMessage } from './dto/message.dto'
@Injectable()
export class AppService {
    constructor(private readonly searchService: SearchService){
      this.checkIndexExisted(INDEX_NAME).then((existed) => {
        if(!existed){
             this.searchService.createIndex(INDEX_NAME, MAPPING)
        }
      })
    }

  async indexProductSearchedData(searchedData: ProductSearchedMessage): Promise<Boolean> {
    try{
      const result = await this.searchService.indexData(INDEX_NAME, searchedData)
      console.log(result)
      return !!result
    } catch(e){
      console.log("indexProductSearchedData error->",e)
      return false
    }
  }

  async checkIndexExisted(indexName: string): Promise<Boolean>{
    try{
      return await this.searchService.checkIndexExisted(indexName)
    } catch(e){
        console.log("checkIndexExisted error ->", e)
        return false
    }
  }
}
