- High Level Design Diagram: https://drive.google.com/file/d/16FQfUqzJFtA9cXO8PNxjkuOAGJnE1t2V/view?usp=sharing
- Search Product Flow: https://drive.google.com/file/d/1awubE37TF0dByrmCd7F8L_zcYzKzY3LG/view?usp=sharing
- Entity Relationship: https://drive.google.com/file/d/14yROb8Ng1K50kyboRgMnY-puPyyTSi7o/view?usp=sharing
- Software development principles:
    + SOLID
    + IoC (IoC Container)
- Software development design: 
    + Domain Driven Design
    + Clean Architure
    + Event Driven Architecture
- Pattern:
    + Microservice
    + CQRS/Event Sourcing
    + Sagas
- Framework:
    + Nestjs
- Pratice: 
    + Optimistic locking
- Test:
    + Jest
- Database: 
    + Mongodb
    + Elasticsearch
- How to run:
    + Install docker
    + run command "docker-compose up --build"
- CURL commands: (postman collection: https://drive.google.com/file/d/1m_hed-OJiYp7UVweYywvGl0g5e-JZ68i/view?usp=sharing)
    - product: 
        + Create Product:
            - uri: POST http://localhost:3000/products
            - body : 
                - name: string
                - price: number
                - colors: [string]
        + Filter product: 
            - uri: GET http://localhost:3000/products/filter
            - query: 
                - filter?:
                    - name?: string
                    - price?: number
                    - colors?: [string]
                - sort:
                    - createAt?: 1| -1
                    - price?: 1| -1
                - cursor: string (pagination)
        + Search by name: 
            - uri: GET http://localhost:3000/products/searchByName
            - query: 
                - filter?:
                    - price?: number
                    - colors?: [string]
                - sort:
                    - createAt?: 1| -1
                    - price?: 1| -1
                - cursor: string (pagination)
                - name: string (full text search)
        + Product by id: 
            - uri: GET http://localhost:3000/products/
            - params: 
                - productId: string
    - callcentre:
        + Create Order:
            - uri: POST http://localhost:3001/orders
            - body :
                products: [
                - productId: string
                - quanity: number
                ]
        + Get all orders:
            - uri: GET http://localhost:3000/orders/
    - logging:
        + Get All Logs:
            - uri: GET localhost:9200/product-searched/_search
            -Body: 
                -query:
                    match_all: {}


